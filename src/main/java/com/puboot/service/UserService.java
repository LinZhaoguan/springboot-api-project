package com.puboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.puboot.entity.User;

/**
 * Created with IntelliJ IDEA.
 * User: linzhaoguan
 * Date: 2019-08-30
 * Time: 4:39 下午
 */
public interface UserService extends IService<User> {
}
