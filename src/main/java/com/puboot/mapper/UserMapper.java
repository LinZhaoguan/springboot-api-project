package com.puboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.puboot.entity.User;

/**
 * Created with IntelliJ IDEA.
 * User: linzhaoguan
 * Date: 2019-08-30
 * Time: 4:38 下午
 */
public interface UserMapper extends BaseMapper<User> {
}
